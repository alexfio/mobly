# Cart
**Francisco Chagas Alexandre de Souza Filho**

- O presente código é uma implementação de um carrinho de compras seguindo os requisitos definidos abaixo.
- O projeto faz uso do Redis para manter um cache dos produtos da página inicial, assim como para driver de sessão.
- Como SGBD relacional foi usado o MySQL, no front-end Bootstrap 4.0 e JQuery. Além disso
foi usado PHP 7.2, Laravel e PHPUnit. 

**Requisitos técnicos:**

 - Elaborar um carrinho de compras simples (lista de categorias / lista de produtos / busca / detalhe / carrinho / criação do pedido).

**Requisitos funcionais :**

 - Os produtos devem estar em um banco de dados relacional nos quais devem ser exibidos na lista;

 - Os produtos devem possuir os seguintes atributos : nome, descriçao, imagem, preço, categoria(um produto pode estar em mais de uma categoria) , características (devem ser pré-definidas e associadas ao produto) ;

 - Não é necessário criar um layout elaborado;

 - O carrinho deve ser mantido mesmo se o usuário navegar em outra pagina (nova busca / listagem / ou detalhe do produto );

 - O pedido deve ser salvo no banco de dados contemplando todos os itens do carrinho e os dados do usuario ( Pessoais e de entrega );

 - Não é necessário integração de nenhuma forma de pagamento, apenas gerar o registro do pedido no banco de dados;


# Instruções de Instalação

- Para uma maior facilidade de execução do projeto, o arquivo **.env**
foi adicionado ao git. Esse arquivo já está preparado para o uso com o Docker, **caso queira executar o sistema sem usar o Docker,
altere as configurações do arquivo para os valores adequados, principalmente o endereço de Host do Mysql e do Redis, bem como
usuário e senha.**
         

O projeto pode ser executado com Docker ou sem o Docker.

   - **Com o Docker**:
	   -  Na raiz do projeto execute:
		   - docker-compose up -d
	   - Instalar as dependências do php via composer: 
		   - docker exec -it web composer install
	   - Instalar as dependências de front-end via bower:
		   - docker exec -it web bower --allow-root install
	   - Rodar as migrations e os seeders:
		   - docker exec -it web php artisan **migrate** **--seed**
	  - No navegador digite: **localhost** para acessar o projeto .
          - Para destruir os containers após a execução da aplicação faça: 
            docker-compose down
	  
- **Sem o Docker**:
    - É necessário que os seguintes softwares estejam instalados:
       - **Composer**, **Bower**, **Redis**, **PHP 7.2**, **MySQL 5.7**.
    
    - No mysql **crie um novo banco de dados** chamado mobly
                   
    - Na raiz do projero execute
	     -  composer install
	     -  bower  install
	     -  Rodar as migrations e os seeders:
		   	- php artisan **migrate** **--seed**
	     -  php artisan serve
	- No navegador digite: **localhost:8000** para acessar o projeto .
		   
	 

# Execução dos casos de teste com PHPUnit

Foi escrita uma bateria de testes para a classe do carrinho de compras.
Para executar os testes basta executar o comando abaixo na raiz do projeto:

     vendor/bin/phpunit --testdox

A Bateria é composta pelos seguintes casos de teste: 


  - Adicao de itens no carrinho
  - Quantidade de itens no carrinho
  - Remocao de itens do carrinho
  - Valor total da venda
  - Carrinho vazio
