<?php

namespace Mobly\Services;

use Mobly\Services\CarrinhoCompras;

class CarrinhoComprasDefault implements CarrinhoCompras
{
    private $itensVenda;

    public function __construct()
    {
        $this->itensVenda = [];
    }

    public function addItemVenda(array $itemAdicionar)
    {
        $itemJaAdicionado = false;
        
        foreach ($this->itensVenda as $index => $item) {
            if ($item['uuid'] == $itemAdicionar['uuid']) {
                $itemJaAdicionado = true;
                
                if (isset($this->itensVenda[$index]['quantidade'])) {
                    $this->itensVenda[$index]['quantidade']++;
                } else {
                    $this->itensVenda[$index]['quantidade'] = 2;
                }
                
                break;
            }
        }

        if (!$itemJaAdicionado) {
            $this->itensVenda[] = $itemAdicionar;
        }
    }

    public function removerItemVenda(string $uuidItem)
    {
        $indiceARemover = null;

        foreach ($this->itensVenda as $indice => $item) {
            if ($item['uuid'] == $uuidItem) {
                $indiceARemover = $indice;
                break;
            }
        }

        if (!is_null($indiceARemover)) {
            unset($this->itensVenda[$indiceARemover]);
            $this->itensVenda = array_values($this->itensVenda);
            return true;
        } else {
            throw new \Excpetion('Item Não Encontrado no Carrinho');
        }
    }

    public function getItensVenda()
    {
        return $this->itensVenda;
    }

    public function getValorTotalVenda()
    {
        $total = 0;
        
        foreach ($this->itensVenda as $item) {
            $valor = $item['preco'] * $item['quantidade'];
            $total += $valor;
        }

        return $total;
    }

    public function estaVazio()
    {
        return count($this->itensVenda) == 0;
    }

    public function getQtdItens()
    {
        return count($this->itensVenda);
    }
}
