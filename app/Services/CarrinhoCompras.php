<?php

namespace Mobly\Services;

interface CarrinhoCompras
{
    public function addItemVenda(array $item);
    public function removerItemVenda(string $uuidItem);
    public function getItensVenda() ;
    public function getValorTotalVenda();
    public function estaVazio();
    public function getQtdItens();
}
