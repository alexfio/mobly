<?php

namespace Mobly\ViewComposers;

use Illuminate\View\View;
use Mobly\Persistence\Repositories\CategoriasRepository;

class DetalhesProdutoViewComposer
{
    private $categoriasRepository;
    
    public function __construct(CategoriasRepository $categoriasRepository)
    {
        $this->categoriasRepository = $categoriasRepository;
    }
    
    public function compose(View $view)
    {
        $dados['categorias'] = $this->categoriasRepository
                                    ->getAll();
        
        return $view->with('componentes', $dados);
    }
}
