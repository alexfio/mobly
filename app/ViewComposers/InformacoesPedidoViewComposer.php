<?php

namespace Mobly\ViewComposers;

use Illuminate\View\View;
use Mobly\Persistence\Repositories\EstadosRepository;
use Mobly\Persistence\Repositories\FormasPagamentoRepository;

class InformacoesPedidoViewComposer
{
    private $estadosRepository;
    private $formasPagamentoRepository;
    
    public function __construct(
            EstadosRepository $estadosRepository,
            FormasPagamentoRepository $formasPagamentoRepository
    
    ) {
        $this->estadosRepository = $estadosRepository;
        $this->formasPagamentoRepository = $formasPagamentoRepository;
    }
    
    public function compose(View $view)
    {
        $dados['estados'] = $this->estadosRepository
                                 ->getAll();
        
        $dados['formas_pagamento'] = $this->formasPagamentoRepository
                                 ->getAll();
        
        
        return $view->with('componentes', $dados);
    }
}
