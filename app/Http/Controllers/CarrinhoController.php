<?php

namespace Mobly\Http\Controllers;

use Illuminate\Http\Request;
use Mobly\Persistence\Repositories\ProdutosRepository;
use Mobly\Services\CarrinhoCompras;
use Mobly\Http\Requests\PedidoRequest;
use voku\helper\AntiXSS;
use Illuminate\Support\Facades\Crypt;
use Mobly\Persistence\Repositories\PedidosRepository;

class CarrinhoController extends Controller
{
    private $repositoryProdutos;
    private $repositoryPedidos;
    
    public function __construct(
            ProdutosRepository $repositoryProdutos,
            PedidosRepository  $repositoryPedidos
    
    ) {
        $this->repositoryProdutos = $repositoryProdutos;
        $this->repositoryPedidos = $repositoryPedidos;
    }
    
    public function index(Request $request, CarrinhoCompras $carrinho)
    {
        $dados['carrinho'] = $request->session()->get('carrinho') ?? $carrinho;
        return view('carrinho', $dados);
    }
    
    public function adicionarProdutoAoCarrinho(Request $request, CarrinhoCompras $carrinho)
    {
        $dados['carrinho'] = $request->session()->get('carrinho') ?? $carrinho;
        $produto = $this->repositoryProdutos->getByUuid($request->input('p'));
        $dados['carrinho']->addItemVenda($produto);
        $request->session()->put('carrinho', $dados['carrinho']);
        
        return redirect()->action("CarrinhoController@index");
    }
    
    public function removerDoCarrinho(Request $request, CarrinhoCompras $carrinho)
    {
        try {
            if (!$request->has('p')) {
                return redirect()->action("CarrinhoController@index");
            }
            
            $dados['carrinho'] = $request->session()->get('carrinho') ?? $carrinho;
            $produto = $request->input('p');
            $dados['carrinho']->removerItemVenda($produto);
            $request->session()->put('carrinho', $dados['carrinho']);
            
            return redirect()->action("CarrinhoController@index");
        } catch (Exception $ex) {
            return redirect()->action("CarrinhoController@indexCarrinho");
        }
    }
    
    public function prepararPedido(Request $request, CarrinhoCompras $novoCarrinho)
    {
        $itensQtd = $request->except(['_token']);
        
        if (count($itensQtd) == 0) {
            return redirect()->action("CarrinhoController@produtos");
        }
        
        foreach ($itensQtd as $index => $itemQtd) {
            $uuidProduto = explode(':', $index)[1];
            $uuidProdutos[$uuidProduto] =  $itemQtd;
        }
       
        
        foreach ($uuidProdutos as $uuidProduto => $quantidade) {
            $produto = $this->repositoryProdutos->getByUuid($uuidProduto);
            $produto['quantidade'] = $quantidade;
            $novoCarrinho->addItemVenda($produto);
        }
        
        $request->session()->put('carrinho', $novoCarrinho);
        return redirect()
                ->action("CarrinhoController@verInformacoesPedido");
    }
    
    public function verInformacoesPedido(Request $request)
    {
        $dados['carrinho'] = $request->session()->get('carrinho');
        return view('informacoes_pedido', $dados);
    }
    
    public function processarPedido(PedidoRequest $request, AntiXSS $antiXSS)
    {
        $dados = $request->except('_token');
        $dados['carrinho'] = $request->session()->get('carrinho');
        
        //Higienizando os dados
        $dados['nome'] = $antiXSS->xss_clean($dados['nome']);
        
        $dados['telefone_residencial'] =
                $antiXSS->xss_clean($dados['telefone_residencial']);
        
        $dados['telefone_celular'] =
                $antiXSS->xss_clean($dados['telefone_celular']);
        
        $dados['cpf'] =
                $antiXSS->xss_clean($dados['cpf']);
        
        $dados['forma_pagamento'] =
                $antiXSS->xss_clean($dados['forma_pagamento']);
        
        $dados['endereco_completo'] =
                $antiXSS->xss_clean($dados['endereco_completo']);
        
        $dados['estado'] =
                $antiXSS->xss_clean($dados['estado']);
        
        $dados['cidade'] =
                $antiXSS->xss_clean($dados['cidade']);
        
        $dados['numero'] =
                $antiXSS->xss_clean($dados['numero']);
        
        $dados['cep'] =
                $antiXSS->xss_clean($dados['cep']);
        
     
        // retirando máscaras...
        
        $dados['telefone_residencial'] =
            \preg_replace("/[^0-9]/", "", $dados['telefone_residencial']);
        
        $dados['telefone_celular'] =
            \preg_replace("/[^0-9]/", "", $dados['telefone_celular']);
        
        $dados['cpf'] =
            \preg_replace("/[^0-9]/", "", $dados['cpf']);
        
        $dados['cep'] =
            \preg_replace("/[^0-9]/", "", $dados['cep']);
        
        
        $dados['telefone_celular'] =
                $antiXSS->xss_clean($dados['telefone_celular']);
        
        
        if (isset($dados['numero_cartao'])) {
            $dados['numero_cartao'] =
                    $antiXSS->xss_clean($dados['numero_cartao']);
            
            $dados['numero_cartao'] =
                    \preg_replace("/[^0-9]/", "", $dados['numero_cartao']);
    
            $dados['numero_cartao'] =
                    Crypt::encryptString($dados['numero_cartao']);
        }
        
        $this->repositoryPedidos->save($dados);
        
        $request->session()->invalidate();
        $request->session()
                ->flash('mensagem_sucesso_compra', 'Compra realizada com sucesso !');
        
        return redirect()
                ->action('HomeController@index');
    }
}
