<?php

namespace Mobly\Http\Controllers;

use Illuminate\Http\Request;
use Mobly\Persistence\Repositories\ProdutosRepository;
use Mobly\Persistence\Repositories\CategoriasRepository;
use voku\helper\AntiXSS;
use Illuminate\Support\Facades\Redis;

class HomeController extends Controller
{
    private $repositoryProdutos;

    public function __construct(ProdutosRepository $repositoryProdutos)
    {
        $this->repositoryProdutos = $repositoryProdutos;
    }

    public function index()
    {
        if (Redis::exists('listas:produtos:tela_inicial')) {
            $tamanhoLista = Redis::lLen('listas:produtos:tela_inicial');
            $dadosSerializados['produtos'] =
                    Redis::lRange('listas:produtos:tela_inicial', 0, $tamanhoLista);
            
            $dados['produtos'] = [];
            foreach ($dadosSerializados['produtos'] as $produto) {
                $dados['produtos'][] = unserialize($produto);
            }
        } else {
            $dados['produtos'] = $this->repositoryProdutos->getAll();
            foreach ($dados['produtos'] as $produto) {
                $produtoSerializado = serialize($produto);
                Redis::rPush('listas:produtos:tela_inicial', $produtoSerializado);

                // A lista no Redis expira em 5 minutos;
                Redis::expire('listas:produtos:tela_inicial', 300);
            }
        }

        return view('home', $dados);
    }

    public function verDetalhesProduto($uuid)
    {
        $dados['produto'] = $this->repositoryProdutos->getByUuid($uuid);
        return view('detalhes_produto', $dados);
    }

    public function verCarrinhoCompras()
    {
        return view('carrinho');
    }

    public function buscarPorNome(Request $request, AntiXSS $antiXSS)
    {
        $nome = $antiXSS->xss_clean($request->input('nome'));

        if (!isset($nome)) {
            return redirect()->action("HomeController@index");
        }

        $dados['produtos'] = $this->repositoryProdutos->getByNome($nome);
        $dados['nome'] = $nome;
        return view('home', $dados);
    }

    public function exibirProdutosPorCategoria($uuid)
    {
        $produtos = $this->repositoryProdutos->getByCategoria($uuid);
        $dados['produtos'] = $produtos;

        return view('home', $dados);
    }

    public function verCategorias(CategoriasRepository $categoriasRepository)
    {
        $dados['categorias'] = $categoriasRepository->getAll();
        return view('categorias', $dados);
    }
}
