<?php

namespace Mobly\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PedidoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "nome" => "required",
            "email" => "required|email",
            "telefone_residencial" => "required|telefone_residencial",
            "telefone_celular" => "required|telefone_celular",
            "cpf" => "required|cpf_valido",
            "endereco_completo" => "required",
            "estado" => "required",
            "cidade" => "required",
            "numero" => "required",
            "numero_cartao" => "required_if:forma_pagamento,1|cartao_credito",
            "cep" => "required|cep_valido",
            "forma_pagamento" => "required",
            
        ];
    }
    
    public function messages()
    {
        return [
            "cpf.cpf_valido" => "O cpf submetido é inválido",
            "telefone_residencial.telefone_residencial"
                             => "O telefone residencial não obedece ao padrão (99)9999-9999.",
            "telefone_celular.telefone_celular"
                             => "O telefone celular não obedece ao padrão (99)99999-9999.",
            "cep.cep_valido"
                             => "O cep não obedece ao padrão 99999-999.",
            "numero_cartao.cartao_credito" => "O cartão de crédito não obedece ao padrão xxxx-xxxx-xxxx-xxxx",
            "numero_cartao.required_if" => "O cartão de crédito é obrigatório",
            
        ];
    }
}
