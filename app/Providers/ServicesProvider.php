<?php

namespace Mobly\Providers;

use Illuminate\Support\ServiceProvider;
use Mobly\Services\CarrinhoCompras;
use Mobly\Services\CarrinhoComprasDefault;
use Mobly\Helpers\ValidacaoInterface;
use Mobly\Helpers\ValidacaoDefault;
use Mobly\Helpers\TransformadorDadosInterface;
use Mobly\Helpers\TransformadorDadosDefault;

class ServicesProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(CarrinhoCompras::class, CarrinhoComprasDefault::class);
        $this->app->bind(ValidacaoInterface::class, ValidacaoDefault::class);
        $this->app
             ->bind(TransformadorDadosInterface::class, TransformadorDadosDefault::class);
        $this->app->alias(TransformadorDadosInterface::class, 'transformador');
    }
}
