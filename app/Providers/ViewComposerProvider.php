<?php

namespace Mobly\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Mobly\ViewComposers\DetalhesProdutoViewComposer;
use Mobly\ViewComposers\InformacoesPedidoViewComposer;

class ViewComposerProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('detalhes_produto', DetalhesProdutoViewComposer::class);
        View::composer('informacoes_pedido', InformacoesPedidoViewComposer::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
