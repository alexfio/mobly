<?php

namespace Mobly\Providers;

use Illuminate\Support\ServiceProvider;
use Mobly\Helpers\ValidacaoInterface;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(ValidacaoInterface $validacao)
    {
        //Registrando a validação de CPF...
        Validator::extend('cpf_valido', function ($atributo, $valor) use ($validacao) {
            return $validacao->cpfEhValido($valor);
        });
        
        //Registrando a validação de telefone residencial...
        Validator::extend('telefone_residencial', function ($atributo, $valor) use ($validacao) {
            return $validacao->telefoneResidencialValido($valor);
        });
        
        //Registrando a validação de telefone celular...
        Validator::extend('telefone_celular', function ($atributo, $valor) use ($validacao) {
            return $validacao->telefoneCelularValido($valor);
        });
        
        //Registrando a validação de cep_validacao
        Validator::extend('cep_valido', function ($atributo, $valor) use ($validacao) {
            return $validacao->cepValido($valor);
        });
        
        //Registrando a validação de cartão de crédito
        Validator::extend('cartao_credito', function ($atributo, $valor) use ($validacao) {
            return $validacao->cartaoCreditoValido($valor);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
