<?php

namespace Mobly\Providers;

use Illuminate\Support\ServiceProvider;
use Mobly\Persistence\Repositories\ProdutosRepository;
use Mobly\Persistence\Eloquent\Repositories\ProdutosEloquentRepository;
use Mobly\Persistence\Repositories\CategoriasRepository;
use Mobly\Persistence\Eloquent\Repositories\CategoriasEloquentRepository;
use Mobly\Persistence\Repositories\EstadosRepository;
use Mobly\Persistence\Eloquent\Repositories\EstadosEloquentRepository;
use Mobly\Persistence\Repositories\FormasPagamentoRepository;
use Mobly\Persistence\Eloquent\Repositories\FormasPagamentoEloquentRepository;
use Mobly\Persistence\Repositories\PedidosRepository;
use Mobly\Persistence\Eloquent\Repositories\PedidosEloquentRepository;

class RepositoriesProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ProdutosRepository::class, ProdutosEloquentRepository::class);
        $this->app->bind(CategoriasRepository::class, CategoriasEloquentRepository::class);
        $this->app->bind(EstadosRepository::class, EstadosEloquentRepository::class);
        $this->app->bind(FormasPagamentoRepository::class, FormasPagamentoEloquentRepository::class);
        $this->app->bind(PedidosRepository::class, PedidosEloquentRepository::class);
    }
}
