<?php
namespace Mobly\Persistence\Repositories;

interface ProdutosRepository
{
    public function getAll() : array;
    public function getByUuid(string $uuid) : array;
    public function getByCategoria($uuidCategoria) : array;
    public function getByNome($nome) : array;
}
