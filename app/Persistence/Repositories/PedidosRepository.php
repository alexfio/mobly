<?php
namespace Mobly\Persistence\Repositories;

interface PedidosRepository
{
    public function save(array $dados);
}
