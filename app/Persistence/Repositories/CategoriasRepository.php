<?php
namespace Mobly\Persistence\Repositories;

interface CategoriasRepository
{
    public function getAll() : array;
}
