<?php
namespace Mobly\Persistence\Repositories;

interface FormasPagamentoRepository
{
    public function getAll() : array;
}
