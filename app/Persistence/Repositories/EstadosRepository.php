<?php
namespace Mobly\Persistence\Repositories;

interface EstadosRepository
{
    public function getAll() : array;
}
