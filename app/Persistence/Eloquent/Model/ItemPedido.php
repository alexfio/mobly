<?php

namespace Mobly\Persistence\Eloquent\Model;

use Illuminate\Database\Eloquent\Model;
use Mobly\Persistence\Eloquent\Model\Pedido;
use Mobly\Persistence\Eloquent\Model\Produto;

class ItemPedido extends Model
{
    protected $dates = ['created_at', 'updated_at'];
   
    public function produto()
    {
        return $this->belongsTo(Produto::class, 'produto_id', 'id');
    }
   
    public function pedido()
    {
        return $this->belongsTo(Pedido::class, 'pedido_id', 'id');
    }
}
