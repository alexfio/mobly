<?php

namespace Mobly\Persistence\Eloquent\Model;

use Illuminate\Database\Eloquent\Model;
use Mobly\Persistence\Eloquent\Model\Pedido;

class FormaPagamento extends Model
{
    public $timestamps = false;
    
    public function itens()
    {
        return $this->hasMany(Pedido::class, 'forma_pagamento_id', 'id');
    }
}
