<?php

namespace Mobly\Persistence\Eloquent\Model;

use Mobly\Persistence\Eloquent\Model\Pedido;
use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    public $timestamps = false;
    
    public function pedidos()
    {
        return $this->hasMany(Pedido::class, "cliente_id", "id");
    }
}
