<?php

namespace Mobly\Persistence\Eloquent\Model;

use Illuminate\Database\Eloquent\Model;
use Mobly\Persistence\Eloquent\Model\ItemPedido;
use Mobly\Persistence\Eloquent\Model\Cliente;
use Mobly\Persistence\Eloquent\Model\FormaPagamento;
use Mobly\Persistence\Eloquent\Model\Estado;

class Pedido extends Model
{
    protected $dates = ['created_at', 'updated_at'];
    
    public function itens()
    {
        return $this->hasMany(ItemPedido::class, 'pedido_id', 'id');
    }
    
    public function cliente()
    {
        return $this->belongsTo(Cliente::class, 'cliente_id', 'id');
    }
    
    public function formaPagamento()
    {
        return $this->belongsTo(FormaPagamento::class, 'forma_pagamento_id', 'id');
    }
    
    public function estado()
    {
        return $this->belongsTo(Estado::class, 'estado_id', 'id');
    }
}
