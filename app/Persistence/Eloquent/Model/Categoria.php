<?php

namespace Mobly\Persistence\Eloquent\Model;

use Illuminate\Database\Eloquent\Model;
use Mobly\Persistence\Eloquent\Model\Produto;

class Categoria extends Model
{
    public $timestamps = false;
    
    public function produtos()
    {
        return $this->belongsToMany(Produto::class, 'categoria_produto', 'categoria_id', "produto_id");
    }
}
