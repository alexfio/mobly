<?php

namespace Mobly\Persistence\Eloquent\Model;

use Illuminate\Database\Eloquent\Model;
use Mobly\Persistence\Eloquent\Model\Produto;

class Caracteristica extends Model
{
    public $timestamps = false;
    
    public function produtos()
    {
        return $this->belongsToMany(Produto::class, 'caracteristica_produto', 'caracteristica_id', "produto_id");
    }
}
