<?php

namespace Mobly\Persistence\Eloquent\Model;

use Illuminate\Database\Eloquent\Model;
use Mobly\Persistence\Eloquent\Model\ItemPedido;
use Mobly\Persistence\Eloquent\Model\Categoria;
use Mobly\Persistence\Eloquent\Model\Caracteristica;

class Produto extends Model
{
    protected $dates = ['created_at', 'updated_at'];
    
    public function itensPedido()
    {
        return $this->hasMany(ItemPedido::class, 'produto_id', 'id');
    }
    
    public function categorias()
    {
        return $this->belongsToMany(Categoria::class);
    }
    
    public function caracteristicas()
    {
        return $this->belongsToMany(Caracteristica::class)->withPivot('valor');
        ;
    }
}
