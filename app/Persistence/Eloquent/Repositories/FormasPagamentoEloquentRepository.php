<?php

namespace Mobly\Persistence\Eloquent\Repositories;

use Mobly\Persistence\Repositories\FormasPagamentoRepository;
use Mobly\Persistence\Eloquent\Model\FormaPagamento;

class FormasPagamentoEloquentRepository implements FormasPagamentoRepository
{
    public function getAll(): array
    {
        return FormaPagamento::all()->toArray();
    }
}
