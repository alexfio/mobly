<?php

namespace Mobly\Persistence\Eloquent\Repositories;

use Illuminate\Support\Facades\DB;
use Mobly\Persistence\Eloquent\Model\Cliente;
use Ramsey\Uuid\Uuid;
use Mobly\Persistence\Repositories\PedidosRepository;
use Mobly\Persistence\Eloquent\Model\FormaPagamento;
use Mobly\Persistence\Eloquent\Model\Pedido;
use Mobly\Persistence\Eloquent\Model\Estado;
use Mobly\Persistence\Eloquent\Model\ItemPedido;
use Mobly\Persistence\Eloquent\Model\Produto;

class PedidosEloquentRepository implements PedidosRepository
{
    public function save(array $dados)
    {
        DB::transaction(function () use ($dados) {
            $cliente = Cliente::where('cpf', $dados['cpf'])->first();
            $formaPagamento = FormaPagamento::where('id', $dados['forma_pagamento'])->first();
            $estado = Estado::where('uuid', $dados['estado'])->first();
            
            if (!$cliente) {
                $cliente = new Cliente();
                $cliente->uuid = Uuid::uuid4()->toString();
            }
            
            $cliente->nome = $dados['nome'];
            $cliente->email = $dados['email'];
            $cliente->telefone_residencial = $dados['telefone_residencial'];
            $cliente->telefone_celular = $dados['telefone_celular'];
            $cliente->cpf = $dados['cpf'];
            $cliente->save();
             
            $pedido = new Pedido();
            $pedido->uuid = Uuid::uuid4()->toString();
            $pedido->numero = $dados['numero'];
            $pedido->cidade = $dados['cidade'];
            $pedido->cep = $dados['cep'];
            $pedido->endereco_completo = $dados['endereco_completo'];
            $pedido->cartao_credito = $dados['numero_cartao'] ?? null;
            
            $pedido->estado()->associate($estado);
            $pedido->formaPagamento()->associate($formaPagamento);
            $pedido->cliente()->associate($cliente);
            $pedido->save();
            
            $carrinho = $dados['carrinho'];
    
            foreach ($carrinho->getItensVenda() as $item) {
                $itemPedido = new ItemPedido();
                $produto = Produto::where('uuid', $item['uuid'])->firstOrFail();
                
                $itemPedido->uuid = Uuid::uuid4()->toString();
                $itemPedido->quantidade = $item['quantidade'];
                $itemPedido->preco_venda = $item['preco'];
                
                $itemPedido->produto()->associate($produto);
                $itemPedido->pedido()->associate($pedido);
                
                $itemPedido->save();
            }
        });
    }
}
