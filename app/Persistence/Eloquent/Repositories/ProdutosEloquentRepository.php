<?php

namespace Mobly\Persistence\Eloquent\Repositories;

use Mobly\Persistence\Repositories\ProdutosRepository;
use Mobly\Persistence\Eloquent\Model\Produto;

class ProdutosEloquentRepository implements ProdutosRepository
{
    public function getAll(): array
    {
        return Produto::all()->toArray();
    }
    
    public function getByUuid(string $uuid) : array
    {
        return Produto::with(['categorias','caracteristicas'])
                ->where('uuid', $uuid)
                ->firstOrFail()
                ->toArray();
    }
    
    public function getByCategoria($uuidCategoria) : array
    {
        $produtos = Produto::whereHas(
        
            'categorias',
                function ($categorias) use ($uuidCategoria) {
                    $categorias->where('uuid', $uuidCategoria);
                }
        
        )->get();
        
        return $produtos->toArray();
    }
    
    public function getByNome($nome) : array
    {
        $nome = '%' . \preg_replace('/\s+/', '%', $nome) . '%';
        return Produto::where('nome', 'like', $nome)->get()->toArray();
    }
}
