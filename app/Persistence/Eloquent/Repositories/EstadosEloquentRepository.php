<?php

namespace Mobly\Persistence\Eloquent\Repositories;

use Mobly\Persistence\Eloquent\Model\Estado;
use Mobly\Persistence\Repositories\EstadosRepository;

class EstadosEloquentRepository implements EstadosRepository
{
    public function getAll(): array
    {
        return Estado::all()->toArray();
    }
}
