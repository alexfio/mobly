<?php

namespace Mobly\Persistence\Eloquent\Repositories;

use Mobly\Persistence\Repositories\CategoriasRepository;
use Mobly\Persistence\Eloquent\Model\Categoria;

class CategoriasEloquentRepository implements CategoriasRepository
{
    public function getAll(): array
    {
        return Categoria::all()->toArray();
    }
}
