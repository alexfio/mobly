<?php

namespace Mobly\Helpers;

interface TransformadorDadosInterface
{
    public function converterPadraoMonetarioBrasil(float $valor) : string;
}
