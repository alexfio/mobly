<?php

namespace Mobly\Helpers;

use Mobly\Helpers\ValidacaoInterface;
use Ramsey\Uuid\Uuid;

class ValidacaoDefault implements ValidacaoInterface
{
    public function uuidEhValido(string $uuid) : bool
    {
        return Uuid::isValid($uuid);
    }

    public function cpfEhValido(string $cpf): bool
    {
        $cpf = preg_replace("/[^0-9]/", "", $cpf);
        
        if (strlen($cpf) != 11 || $cpf == '00000000000' || $cpf == '11111111111' || $cpf == '22222222222' || $cpf == '33333333333' || $cpf == '44444444444' || $cpf == '55555555555' || $cpf == '66666666666' || $cpf == '77777777777' || $cpf == '88888888888' || $cpf == '99999999999') {
            return false;
        } else {
            for ($t = 9; $t < 11; $t++) {
                for ($d = 0, $c = 0; $c < $t; $c++) {
                    $d += $cpf{$c} * (($t + 1) - $c);
                }

                $d = ((10 * $d) % 11) % 10;

                if ($cpf{$c} != $d) {
                    return false;
                }
            }

            return true;
        }
    }
    
    public function telefoneResidencialValido(string $tel = '') : bool
    {
        return  strlen($tel) == 0  ||
                preg_match('/^\([0-9]{2}\)[0-9]{4}\-[0-9]{4}$/', $tel);
    }
    
    public function telefoneCelularValido(string $cel = '') : bool
    {
        return   strlen($cel) == 0 ||
                preg_match('/^\([0-9]{2}\)[0-9]{5}\-[0-9]{4}$/', $cel);
    }
    
    public function cepValido(string $cep = '') : bool
    {
        return   strlen($cep) == 0 ||
                preg_match('/^[0-9]{5}\-[0-9]{3}$/', $cep);
    }

    public function cartaoCreditoValido(string $cartao = ''): bool
    {
        return  strlen($cartao) == 0  ||
                preg_match('/^[0-9]{4}\-[0-9]{4}\-[0-9]{4}\-[0-9]{4}$/', $cartao);
    }
}
