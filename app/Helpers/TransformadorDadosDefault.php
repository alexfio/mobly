<?php

namespace Mobly\Helpers;

use Mobly\Helpers\TransformadorDadosInterface;

class TransformadorDadosDefault implements TransformadorDadosInterface
{
    public function converterPadraoMonetarioBrasil(float $valor): string
    {
        return \number_format($valor, 2, ",", ".");
    }
}
