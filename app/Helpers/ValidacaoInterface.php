<?php

namespace Mobly\Helpers;

interface ValidacaoInterface
{
    public function cpfEhValido(string $cpf) : bool;
    public function uuidEhValido(string $uuid) : bool;
    public function telefoneResidencialValido(string $tel) : bool;
    public function telefoneCelularValido(string $cel) : bool;
    public function cepValido(string $cep) : bool;
    public function cartaoCreditoValido(string $cartao) : bool;
}
