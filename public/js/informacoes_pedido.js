(function(){
   $(document).ready(function () {
                $('#cpf').mask('999.999.999-99');
                $('#cep').mask('00000-000');
                $('#telefone_residencial').mask('(99)9999-9999');
                $('#telefone_celular').mask('(99)99999-9999');
                $('#numero_cartao').mask('9999-9999-9999-9999');
                
                registrarEventos();

            });
            
})();

function registrarEventos() {
   $('#forma_pagamento').on('change', function(){
       let forma_pagamento = $(this);
       if(forma_pagamento.val() == '1' ) {
           $('#numero_cartao').attr('disabled', false);
           $('#numero_cartao').attr('required', true);
       }
       else {
           $('#numero_cartao').val('');
           $('#numero_cartao').attr('disabled', true);
           $('#numero_cartao').attr('required', false);
       }
       
   });
   
   $('#form-pedido').on('submit', function(){
       let botao = $("#botao-cadastrar-pedido");
       botao.prop('disabled', true);
   });
   
   
}
