@extends('template')

@section('conteudo')

<div class ='container-fluid borda_superior_conteudo animated fadeIn'>
    <br><br>
    <div class ='row'>
        <div class ='col-lg-12'>
            <h2 class = 'text-center text-uppercase text-info'>
                Finalização do Pedido
            </h2>
        </div>
    </div>
    <br><br>
    <div class ='row'>
        <div class ='col-lg-12'> 
            <div class ='row'>
                <div class ='col-lg-10 offset-lg-1'>
                    <a  href ="{{route('carrinho')}}"
                        class = "btn btn-info text-white">
                        <i class = "fa fa-chevron-left"></i>
                        Voltar
                    </a>
                </div>
            </div>
            
            <br>
            
            <div class ='row'>
                    <div class ='col-lg-10 offset-lg-1'>
                        <table class ='table table-striped '>
                            <thead class ='bg-info thead-inverse text-white'>
                                <tr>
                                    <th>
                                        <i class ='fa fa-cart-arrow-down'></i>
                                        Resumo do Pedido
                                    </th>
                                    <th>
                                        Item
                                    </th>
                                    <th>
                                        Quantidade
                                    </th>
                                    <th>
                                        Preço
                                    </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($carrinho->getItensVenda() as $item)
                                    <tr>
                                        <td></td>
                                        <td>  
                                            {{ $item['nome'] }}
                                        </td>
                                        <td>
                                            {{ $item['quantidade'] }}
                                        </td>
                                        <td>
                                            R$ {{ app('transformador')->converterPadraoMonetarioBrasil($item['preco']) }}
                                        </td>
                                        <td></td>
                                </tr>
                                @endforeach
                            </tbody>
                            <tfoot class ='thead-inverse bg-info text-white'>
                                <tr>
                                    <th></th>
                                    <th>
                                        Total
                                    </th>
                                    <th></th>
                                    <th>
                                        R$ {{ app('transformador')->converterPadraoMonetarioBrasil($carrinho->getValorTotalVenda())}}
                                    </th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                        <br>
                    </div>
                </div>
        </div>
    </div> 
    <form id  = "form-pedido" action="{{route('processar-pedido')}}" method="post" >

        <input type = 'hidden' name ='_token' value ='{{csrf_token()}}'>

        <div class ='row'>
            <div class ='offset-lg-2 col-lg-8'>
                <h3 class = 'text-center text-uppercase text-info'>
                    Dados Cliente
                </h3>
                <br>
               @if($errors->any())
                <div class = "alert alert-danger">
                    <p> Os seguintes campos apresentaram problemas: </p>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
               @endif
               <br>
                <div class ='row'>
                    <div class ='col-lg-8'>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class = "fa fa-user"></i>
                                </div>
                            </div>
                            <input 
                                   name = "nome"
                                   type="text" 
                                   class="form-control" 
                                   id="inlineFormInputGroup" 
                                   placeholder="Nome Completo"
                                   value = "{{old('nome')}}"
                                   required>
                        </div>
                    </div>
                    <div class ='col-lg-4'>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class = "fa fa-envelope"></i>
                                </div>
                            </div>
                            <input 
                                   name ="email"
                                   type="email" 
                                   class="form-control" 
                                   id="inlineFormInputGroup" 
                                   placeholder="Email"
                                   value = "{{old('email')}}"
                                   required>
                        </div>
                    </div>
                </div>
                <div class ='row'>
                    <div class ='col-lg-4'>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class = "fa fa-phone"></i>
                                </div>
                            </div>
                            <input 
                                   id = "telefone_residencial" 
                                   name = "telefone_residencial"
                                   type="text" 
                                   class="form-control" 
                                   id="inlineFormInputGroup" 
                                   placeholder="Telefone Residencial"
                                   value = "{{old('telefone_residencial')}}"
                                   required>
                        </div>
                    </div>
                    <div class ='col-lg-4'>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class = "fa fa-mobile-phone"></i>
                                </div>
                            </div>
                            <input 
                                   id = "telefone_celular" 
                                   name = "telefone_celular"
                                   type="text" 
                                   class="form-control" 
                                   id="inlineFormInputGroup" 
                                   placeholder="Telefone Celular"
                                   value = "{{old('telefone_celular')}}"
                                   required>
                        </div>
                    </div>
                    <div class ='col-lg-4'>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class = "fa fa-credit-card"></i>
                                </div>
                            </div>
                            <input 
                                   name = "cpf"
                                   type="text" 
                                   class="form-control" 
                                   id="cpf" 
                                   placeholder="CPF"
                                   value = "{{old('cpf')}}"
                                   required>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br><br>
        <div class ='row'>
            <div class ='offset-lg-2 col-lg-8'>
                <h3 class = 'text-center text-uppercase text-info'>
                    Forma de Pagamento
                </h3>
                <br>
               <select class="form-control" 
                                    name = "forma_pagamento" 
                                    id="forma_pagamento" 
                                    required>

                                <option value = "">
                                    Forma de Pagamento
                                </option>
                                @foreach($componentes['formas_pagamento'] as $forma_pagamento)
                                    <option 
                                        {{ $forma_pagamento['id'] == old('forma_pagamento') ? "selected" : ''}}
                                        value = "{{$forma_pagamento['id']}}">
                                        
                                        {{$forma_pagamento['nome']}}
                                        
                                    </option>
                                @endforeach    
                            </select>
                <br>
                  <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class = "fa fa-credit-card"></i>
                                </div>
                            </div>
                            <input 
                                   id = "numero_cartao"
                                   name = "numero_cartao" 
                                   type="text" 
                                   class="form-control" 
                                   id="inlineFormInputGroup" 
                                   placeholder="Número do Cartão"
                                   value = "{{old('numero_cartao')}}"
                                   {{ old('numero_cartao') ? '' : 'disabled'  }} >
                        </div>
                
            </div>
        </div>
        <br><br>
        <div class ='row'>
            <div class ='offset-lg-2 col-lg-8'>
                <h3 class = 'text-center text-uppercase text-info'>
                    Endereço de Entrega
                </h3>
                <br>
                <div class ='row'>
                    <div class ='col-lg-12'>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class = "fa fa-home"></i>
                                </div>
                            </div>
                            <input 
                                   name = "endereco_completo"
                                   type="text" 
                                   class="form-control" 
                                   id="inlineFormInputGroup" 
                                   placeholder="Endereço Completo"
                                   value = "{{old('endereco_completo')}}"
                                   required>
                        </div>
                    </div>
                </div>
                <div class ='row'>
                    <div class ='col-lg-3'>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class = "fa fa-map-marker"></i>
                                </div>
                            </div>
                            <select class="form-control" 
                                    name = "estado" 
                                    id="inlineFormInputGroup" 
                                    required>

                                <option value = "">
                                    Estado
                                </option>
                                @foreach($componentes['estados'] as $estado)
                                    <option 
                                        {{ $estado['uuid'] == old('estado') ? "selected" : ''}}
                                        value = "{{$estado['uuid']}}">
                                        
                                        {{$estado['nome']}}
                                        
                                    </option>
                                @endforeach    
                            </select>
                        </div>
                    </div>
                    <div class ='col-lg-3'>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class = "fa fa-map-marker"></i>
                                </div>
                            </div>
                            <input 
                                   name = "cidade" 
                                   type="text" 
                                   class="form-control" 
                                   id="inlineFormInputGroup" 
                                   placeholder="Cidade"
                                   value = "{{old('cidade')}}"
                                   required>
                        </div>
                    </div>
                    <div class ='col-lg-3'>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class = "fa fa-home"></i>
                                </div>
                            </div>
                            <input 
                                   name ="numero"
                                   type="text" 
                                   class="form-control" 
                                   id="inlineFormInputGroup" 
                                   placeholder="Número"
                                   value = "{{old('numero')}}"
                                   required>
                        </div>
                    </div>
                    <div class ='col-lg-3'>
                        <div class="input-group mb-2">
                            <div class="input-group-prepend">
                                <div class="input-group-text">
                                    <i class = "fa fa-envelope"></i>
                                </div>
                            </div>
                            <input type="text" 
                                   id = "cep"
                                   name ="cep"
                                   class="form-control" 
                                   
                                   placeholder="CEP"
                                   value = "{{old('cep')}}"
                                   required> 
                        </div>
                    </div>
                </div>
                <br>
                <br>
                <div class ='row'>
                    <div class ='col-lg-12'>
                        <button id = "botao-cadastrar-pedido" class = "btn btn-info btn-block" type = "submit">
                            <i class = "fa fa-check"></i>
                            Finalizar Pedido
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <br><br><br>
    </form>
</div>
@endsection
