
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

<link rel="stylesheet" 
      href="{{url('lib/bootstrap/dist/css/bootstrap.min.css')}}"/>

<link rel="stylesheet" 
      href="{{url('lib/font-awesome/css/font-awesome.min.css')}}"/>


<link rel="stylesheet" 
      href="{{url('lib/animate.css/animate.min.css')}}" type="text/css">

<link rel="stylesheet" 
      href="{{url('css/style.css')}}"/>      
