@extends('template')

@section('conteudo') 
<div class ='container-fluid animated fadeIn'> 
    
    <form action = "{{route('buscar-por-nome')}}" method="post">
        <input type = "hidden" name ="_token" value = "{{csrf_token()}}">
                       
        <div class ="row mt-3">

            <div class = "offset-1 col-lg-8">    
                <input class ="form-control" 
                       name = "nome" type="text" 
                       value ="{{$nome or '' }}"
                       placeholder="Busca por nome"
                       required>
            </div>
            <div class = "col-lg-2">    
                <button  class = "btn btn-info btn-block" type = "submit">
                    <i class = "fa fa-search"></i>
                    Buscar
                </button>
            </div>
        </div>
    </form>

    @if(isset($nome))
        <div class ="row mt-3">
            <div class = "col-lg-12">
                <h4>Resultado da Pesquisa</h4>
            </div>
        </div>
    @endif
    
    @if(session()->has('mensagem_sucesso_compra'))
    <br>
        <div class ='col-lg-5 offset-lg-3 text-center'>
            <div class ="alert alert-info">
                <span> 
                    <i class ='fa fa-check'></i>
                    {{ session()->get('mensagem_sucesso_compra') }}
                    
                </span>
            </div>
        </div>
    @endif

    <div class ="row mt-3">

        @forelse($produtos as $produto)
        <div class ="col-lg-4 mt-3 ">
            <div class ="card ">
                <img  class ="card-img-top img-fluid img_card_curso d-block justify-content-center imagem_produto_zoom"
                      
                      src = "{{$produto['imagem']}}">
                
                <div class ="card-body justify-content-center">
                    
                    <div class ="row">
                        <div class ="col-lg-12">
                            <p>{{$produto['nome']}}</p>
                        </div>
                    </div>

                    <div class ="row">
                        <div class ="col-lg-12">
                            <p>R$ {{ app('transformador')->converterPadraoMonetarioBrasil($produto['preco'])}}</p>
                        </div>
                    </div> 

                    <div class ="row">
                        <div class ="col-lg-12">
                            <a  href="{{route('produtos-detalhes', [$produto['uuid']])}}"
                                class = "btn btn-info btn-block text-white">
                                    Ver Mais
                            </a>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        @empty
            
        <div class ='col-lg-5 offset-lg-3 text-center'>
            <div class ="alert alert-info">
                <span>
                    <i class ='fa fa-search'></i>
                    Nenhum resultado encontrado
                    
                </span>
            </div>
        </div>

        @endforelse
        
    </div>
</div>
@endsection