@extends('template')

@section('conteudo') 
<div class ='container-fluid animated fadeIn'> 
    
    <form action = "{{route('buscar-por-nome')}}" method="post">
        <input type = "hidden" name ="_token" value = "{{csrf_token()}}">
                       
        <div class ="row mt-3">

            <div class = "offset-1 col-lg-8">    
                <input class ="form-control" 
                       name = "nome" type="text" 
                       value ="{{$nome or '' }}"
                       placeholder="Busca por nome"
                       required>
            </div>
            <div class = "col-lg-2">    
                <button  class = "btn btn-info btn-block" type = "submit">
                    <i class = "fa fa-search"></i>
                    Buscar
                </button>
            </div>
        </div>
    </form>

    <div class ="row mt-3">
        <div class ="offset-lg-1 col-lg-10">
            <ul class="list-group">
                <li class="list-group-item bg-info text-white">
                    Categorias
                </li>
                @foreach($categorias as $categoria)
                <li class="list-group-item">
                    <a href = '{{ route('buscar-produtos-categoria', $categoria['uuid']) }}' class ='nav-link btn-link link_categorias' >
                      {{ $categoria['nome'] }}
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>
@endsection