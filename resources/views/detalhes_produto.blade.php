@extends('template')

@section('conteudo')


<div class ='container-fluid borda_superior_conteudo animated fadeIn'>
    <div class ="container p-5">

        <div class ="row">
            <div class ="col-lg-8 ml-0" >
                
                <div class ='row'>
                    <div class ='col-lg-12'>
                        <h4 class ='mb-4 text-right text-uppercase titulo_produto' >
                            {{$produto['nome']}}
                        </h4>
                    </div>
                </div>

                <div class ="row mb-2">
                    <div class ="col-lg-12 p-1 cabecalho_detalhes" >
                        <div class ="row ">
                            <div class ="col-lg-4">
                                <span class = "cabecalho_categorias">Categorias</span> <br>
                                <span class = "lista_categorias"  >
                                    @for($c = 0; $c < count($produto['categorias']) ; $c++)
                                        @if($c == count($produto['categorias']) - 1)
                                            {{$produto['categorias'][$c]['nome'] }} 
                                        @else
                                            {{$produto['categorias'][$c]['nome'] . ", " }} 
                                        @endif
                                    @endfor
                                </span> 
                            </div>

                            <div class ="col-lg-2">
                                <span class = "cabecalho_preco"  >Preço</span> <br>
                                <span class = "valor_preco" >
                                    R$ {{ app('transformador')->converterPadraoMonetarioBrasil($produto['preco']) }}
                                </span> 
                            </div> 
                               
                            <div class ='offset-lg-2 col-lg-4 '>
                                <form action = "{{ route('adicionar-ao-carrinho') }}" method = "post">
                                    <input type = 'hidden' name ='_token' 
                                           value = '{{csrf_token()}}'>

                                    <input type = 'hidden' name = 'p' 
                                           value = "{{ $produto['uuid'] }}">

                                    <div class ="col-lg-4 mt-1">
                                        <button type = "submit" class ='btn btn-info'>
                                            <i class ='fa fa-cart-arrow-down'></i>
                                            Adicionar ao Carrinho
                                        </button>

                                    </div>
                                </form>   
                            </div>     
                        </div>
                    </div>
                </div> 
                <div class ='row '>
                    <div class ='col-lg-12 '>
                        <img  
                            src = '{{ $produto['imagem'] }}'
                            class = 'img-fluid imagem_produto'>
                    </div>
                </div>
                <div class ='row'>
                    <div class ='col-lg-12'>
                        <h4 class ='mb-4 text-uppercase text-right titulo_informacoes_produto'>
                            Informações sobre o produto   
                        </h4>
                    </div>
                </div>
                <div class = 'row text-justify texto_descricoes'>
                    <div class ='col-lg-12'>
                        <table class ='table container-caracteristicas' >
                            <tbody>
                            <tr>
                                    <td>
                                        Características
                                    </td>
                                    <td class ='text-right'>
                                    
                                    </td>
                            </tr>
                            @foreach($produto['caracteristicas'] as $caracteristica )
                            <tr>
                                <td>
                                     {{$caracteristica['nome']}}
                                </td>
                                <td class ='text-right'>
                                    {{$caracteristica['pivot']['valor']}}
                                </td>
                            </tr>
                                        
                            @endforeach
                            </tbody>
                        </table>   
                    </div>
                    <div class ='col-lg-12'>
                        <p> </p>
                    </div>    
                </div>

                <div class ='row'>
                    <div class ='col-lg-12'>
                        <h4 class ='mb-4 text-right text-uppercase nome_produto' >
                            {{ $produto['nome'] }}
                        </h4>
                    </div>
                </div>

                <div class = 'row text-justify'>
                    <div class ='col-lg-12'>
                        <p class = 'descricao_produto'>
                            {{ $produto['descricao'] }}
                        </p>
                    </div>
                </div>

                <form action = "{{route('adicionar-ao-carrinho')}}" method = "post">

                    <input type = 'hidden' name = '_token' value = '{{csrf_token()}} '>
                    <input type = 'hidden' name = 'p' value = "{{$produto['uuid']}}">

                    <div class = 'row mt-2 mb-3'>
                        <div class ='col-lg-4 offset-lg-8'>
                            <button type = "submit" class ='btn btn-info btn-block'>
                                <i class = 'fa fa-cart-arrow-down'></i>
                                Adicionar ao Carrinho
                            </button>
                        </div>
                    </div>

                </form>

            </div>

            <div class ="col-lg-4 mt-sm-2 mt-md-0"  >

                <div class ='row mt-2'>
                    <div class ='col-lg-12'>

                        <div class ='row'>
                            <div class ='col-lg-12 text-uppercase p-3 mt-1 text-right' >
                                <h5 class ='text-info'>
                                    Categorias
                                </h5>
                            </div>
                        </div> 

                        <div class ='row'>
                            <div class = 'col-lg-12'>
                                <ul class ='nav flex-column'>
                                    @foreach($componentes['categorias'] as $categoria) 
                                    <li class ='nav-item'>
                                        <a href = '{{ route('buscar-produtos-categoria', $categoria['uuid']) }}' class ='nav-link btn-link nome_categoria' >
                                            {{ $categoria['nome'] }}
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>    

                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
@endsection