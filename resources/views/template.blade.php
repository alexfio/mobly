<!DOCTYPE html>
<html> 
    <head>
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <meta charset="utf-8">
        <meta name="viewport" content = 'width=device-width, initial-scale=1,shrink-to-fit=no'>
       
        @include('style')
       
        <title>
            Mobly - Carrinho de Compras
        </title>

    </head>
    <body>
        <header>            
            @include('barra_superior')
        </header>

        @yield('conteudo')

        @section('scripts')
        
        <script src="{{url('lib/Jquery/dist/jquery.min.js')}}"></script>
        <script src = "{{url('js/jquery-ui/jquery-ui.min.js')}}"></script>
        <script src = "{{url('lib/jquery-mask-plugin/dist/jquery.mask.min.js')}}"></script>
        <script src="{{url('lib/bootstrap/dist/js/bootstrap.min.js')}}" ></script>
        <script src = "{{url('js/informacoes_pedido.js')}}"></script>
        
        
        @show

    </body>
</html>