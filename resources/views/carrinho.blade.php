@extends('template')

@section('conteudo')
<div class ='container-fluid borda_superior_conteudo animated fadeIn'>
    <br><br>
    
    <h2 class = 'text-center text-uppercase text-info'>
        Carrinho de Compras
    </h2>
    
    <br>
    
    @if($carrinho->estaVazio())
    
    <div class ='row'>
        <div class ='col-lg-8 offset-lg-2'>
            <div class ="alert alert-info">
                <p>
                    <i class ='fa fa-cart-arrow-down'></i>
                    Você ainda não adicionou nenhum produto em seu carrinho. 
                    Veja a 
                    <a class = "text-info"  href = "{{route('produtos')}}" >
                        <b>relação completa de produtos</b>
                    </a>.
                </p>
            </div>
        </div>
    </div>

    @else
    
    <form id ="form" method = 'post' action = '{{route('preparar-pedido')}}' >
        <input type = 'hidden' name ='_token' value ='{{csrf_token()}}'>

        <div class ='row'>
            <div class ='col-lg-12'>
                <table class ='table table-striped table-hover'>
                    <thead class ='bg-info thead-inverse text-white'>
                        <tr>
                            <th>
                                <i class ='fa fa-cart-arrow-down'></i>
                                Carrinho 
                            </th>
                            <th>
                                Item
                            </th>
                            <th>
                                Quantidade
                            </th>
                            <th>
                                Preço
                            </th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($carrinho->getItensVenda() as $item)
                         <tr>
                            <td>
                                <img 
                                    src = '{{$item['imagem']}}'
                                    class = 'img-fluid'
                                    width = '150'>
                            </td>
                            <td>
                                {{$item['nome']}}
                            <td>
                                <input 
                                    class ='form-control input_quantidade_itens' 
                                    type="number" min = "1"
                                    name ="qtd:{{$item['uuid']}}"
                                    value ="{{ $item['quantidade'] ?? 1 }}"
                                    required>
                            </td>
                            <td>
                                R$ {{$item['preco']}}
                            </td>
                            <td>  
                                <a  href="{{route('remover-do-carrinho')}}?p={{$item['uuid']}}"
                                    title="Remover do Carrinho"
                                    class = 'btn  btn-danger text-white'>
                                    <i class ='fa fa-trash-o'></i>

                                </a>
                            </td>
                        </tr>
                        @endforeach
                    
                    </tbody>
                    <tfoot class ='thead-inverse bg-info text-white'>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </tfoot>
                </table>

                <div class = 'row'>
                    <div class ='col-lg-2'>
                        <button 
                            id = "btnInscricao"
                            class = 'btn btn-info btn-lg'
                            type ='submit'>
                            
                            <span>Finalizar Pedido
                             <i class = 'fa fa-chevron-right'></i>
                            </span>

                        </button>

                    </div>
                </div>
                <br><br>
            </div>
        </div>
    </form>

    @endif

</div>
@endsection
