<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mobly\Services\CarrinhoComprasDefault;
use Ramsey\Uuid\Uuid;

class CarrinhoComprasTest extends TestCase
{
    private $carrinho;
    
    public function setUp() {
        $this->carrinho = new CarrinhoComprasDefault();
        parent::setUp();
    }
    
    public function testAdicaoDeItensNoCarrinho()
    {   
        //Montagem do cenário
        $itemAdicionar['uuid'] = Uuid::uuid4()->toString();
        $itemAdicionar['nome'] = "Produto Número 1";
        $itemAdicionar['preco'] = 123.50;
        
              
        $this->carrinho->addItemVenda($itemAdicionar);
        $item = $this->carrinho->getItensVenda()[0]; 
        
        $this->assertContains('Produto Número 1', $item);
        
    
    }
    
    
    public function testQuantidadeDeItensNoCarrinho()
    {   
        //Montagem do cenário
        $itemAdicionar['uuid'] = Uuid::uuid4()->toString();
        $itemAdicionar['nome'] = "Produto Número 1";
        $itemAdicionar['preco'] = 123.50;
        
        $this->carrinho->addItemVenda($itemAdicionar);
       
        $itemAdicionar['uuid'] = Uuid::uuid4()->toString();
        $itemAdicionar['nome'] = "Produto Número 2";
        $itemAdicionar['preco'] = 620;
                
        $this->carrinho->addItemVenda($itemAdicionar);
        $qtd = $this->carrinho->getQtdItens();
        
        $this->assertEquals(2, $qtd);
        
        
        
    }
    
    /**
     * @depends testQuantidadeDeItensNoCarrinho
     * 
     */
    public function testRemocaoDeItensDoCarrinho()
    {   
        //Montagem do cenário
        $itemAdicionar['uuid'] = Uuid::uuid4()->toString();
        $itemAdicionar['nome'] = "Produto Número 1";
        $itemAdicionar['preco'] = 123.50;
        
              
        $this->carrinho->addItemVenda($itemAdicionar);
        $item = $this->carrinho->getItensVenda()[0]; 
        
        $this->assertContains('Produto Número 1', $item);
        
        $this->carrinho->removerItemVenda($itemAdicionar['uuid']);
        
        $qtd = $this->carrinho->getQtdItens();
        
        $this->assertEquals(0, $qtd);
        
        
    }
    
    
    
    /**
     * @depends testAdicaoDeItensNoCarrinho
     * 
     */
    public function testValorTotalDaVenda()
    {   
        //Montagem do cenário
        $itemAdicionar['uuid'] = Uuid::uuid4()->toString();
        $itemAdicionar['nome'] = "Produto Número 1";
        $itemAdicionar['quantidade'] = 1;
        $itemAdicionar['preco'] = 123.50;
        
        $this->carrinho->addItemVenda($itemAdicionar);
       
        $itemAdicionar['uuid'] = Uuid::uuid4()->toString();
        $itemAdicionar['nome'] = "Produto Número 2";
        $itemAdicionar['quantidade'] = 2;
        $itemAdicionar['preco'] = 620;
                
        $this->carrinho->addItemVenda($itemAdicionar);
        
        $valorTotal = $this->carrinho->getValorTotalVenda();
        
        $this->assertEquals(1363.5, $valorTotal);
        
        
    }

    public function testCarrinhoVazio()
    {   
        //Montagem do cenário         
        $qtd = $this->carrinho->getQtdItens();        
        $this->assertEquals(0, $qtd);
    }
    
}
