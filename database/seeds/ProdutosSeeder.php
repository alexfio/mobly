<?php

use Illuminate\Database\Seeder;
use Mobly\Persistence\Eloquent\Model\Categoria;
use Mobly\Persistence\Eloquent\Model\Produto;
use Mobly\Persistence\Eloquent\Model\Caracteristica;
use Ramsey\Uuid\Uuid;

class ProdutosSeeder extends Seeder
{
   
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //categorias
        $moveis = Categoria::find(1);
        $eletronicos = Categoria::find(2);
        $eletrodomesticos = Categoria::find(3);
        $banho = Categoria::find(4);
        $decoracao = Categoria::find(5);
        $armario = Categoria::find(6);
        $cozinha = Categoria::find(7);
        
        
        //caracteristica
        $cor = Caracteristica::find(1);
        $altura = Caracteristica::find(3);       
        $largura = Caracteristica::find(4);
        $profundidade = Caracteristica::find(5);
        $peso = Caracteristica::find(6);
        
        //produtos
        $prod = new Produto();
        $prod->uuid = Uuid::uuid4()->toString();
        $prod->nome = "Sofá 3 Lugares Retrátil Arezzo Velveteen Grafite";
        $prod->descricao = "O Sofá 3 Lugares Retrátil Arezzo, além de todas essas funções, ainda é um carinho na sua decoração. Desenhado em formas retilíneas e marcantes, ele uma excelente opção para quem deseja deixar, ao mesmo tempo, a casa mais bonita e confortável.";
        $prod->imagem = "https://staticmobly.akamaized.net/p/Catarina-SofC3A1-3-Lugares-RetrC3A1til-Arezzo-Velveteen-Grafite-1115-123525-1-zoom.jpg";
        $prod->preco = 799.99;
        $prod->save();
        $prod->categorias()->attach($moveis->id);
        $prod->caracteristicas()->attach($cor->id, ['valor' => 'Grafite']);
        $prod->caracteristicas()->attach($altura->id, ['valor' => '93 cm']);
        $prod->caracteristicas()->attach($largura->id, ['valor' => '187 cm']);
        $prod->caracteristicas()->attach($profundidade->id, ['valor' => '123 cm']);
        $prod->caracteristicas()->attach($peso->id, ['valor' => '55,100 kg']);
        
        $prod = new Produto();
        $prod->uuid = Uuid::uuid4()->toString();
        $prod->nome = "Sofá 3 Lugares Retrátil e Reclinável Prius News Suede Tabaco";
        $prod->descricao = "O seu sofá pode ser muito mais do que um simples móvel que compõe a sala de estar, afinal, é importante unir conforto e beleza em um mesmo produto. E se você está procurando uma ótima opção para a sua casa, conheça o Sofá Prius News. Sua estrutura retrátil e reclinável te dá a liberdade de encontrar a posição ideal para ter um descanso completo ou aproveitar para assistir televisão depois de um longo dia de trabalho. Ah, o seu lar-style também sai ganhando, viu? A cor marrom é sofisticada, deixa o ambiente charmoso e é bem fácil de combinar com os outros itens do cômodo. Demais, não é?";
        $prod->imagem = "https://staticmobly.akamaized.net/p/Phormatta-SofC3A1-3-Lugares-RetrC3A1til-e-ReclinC3A1vel-Prius-News-Suede-Tabaco-0217-003525-1-zoom.jpg";
        $prod->preco = 699.99;
        $prod->save();
        $prod->categorias()->attach($moveis->id);
        $prod->caracteristicas()->attach($cor->id, ['valor' => 'Tabaco']);
        $prod->caracteristicas()->attach($altura->id, ['valor' => '93 cm']);
        $prod->caracteristicas()->attach($largura->id, ['valor' => '187 cm']);
        $prod->caracteristicas()->attach($profundidade->id, ['valor' => '123 cm']);
        $prod->caracteristicas()->attach($peso->id, ['valor' => '55,100 kg']);
        
        $prod = new Produto();
        $prod->uuid = Uuid::uuid4()->toString();
        $prod->nome = "Guarda-Roupa Casal com Espelho Mustang 10 Pt 4 Gv Amêndoa";
        $prod->descricao = "Precisando organizar melhor suas peças de roupas? Então você vai se apaixonar pelo Guarda-Roupa Casal com Espelho Mustang. Confeccionado com materiais de qualidade, ele é resistente e promete uma vida útil mais durável. Os nichos auxiliam a organizar também as roupas de cama, travesseiros e até edredons. Demais, né?";
        $prod->imagem = "https://staticmobly.akamaized.net/p/MC3B3veis-Albatroz-Guarda-Roupa-Casal-com-Espelho-Mustang-10-Pt-4-Gv-AmC3AAndoa-0562-493415-1-zoom.jpg";
        $prod->preco = 1419.9;
        $prod->save();
        $prod->categorias()->attach($moveis->id);
        $prod->categorias()->attach($armario->id);
        $prod->caracteristicas()->attach($cor->id, ['valor' => 'Amêndoa']);
        $prod->caracteristicas()->attach($altura->id, ['valor' => '235 cm']);
        $prod->caracteristicas()->attach($largura->id, ['valor' => '240 cm']);
        $prod->caracteristicas()->attach($profundidade->id, ['valor' => '56 cm']);
        $prod->caracteristicas()->attach($peso->id, ['valor' => '188,000 kg']);
        
        
        $prod = new Produto();
        $prod->uuid = Uuid::uuid4()->toString();
        $prod->nome = "Banqueta Alta Fitz Art Dark Marrom";
        $prod->descricao = "Com a Banqueta Fitz, decorar o cômodo será muito simples. Versátil, ela combina perfeitamente com qualquer ambiente: desde o home office até para área gourmet. Para ficar ainda melhor, ela possui ajuste de altura, apoio para os pés e gira até 360°. Ah, destaque também para os detalhes do revestimento nesta linda cor que é fácil de combinar com qualquer decoração. Perfeita, né?";
        $prod->imagem = "https://staticmobly.akamaized.net/p/Mobly-Banqueta-Alta-Fitz-Art-Dark-Marrom-6792-471811-1-zoom.jpg";
        $prod->preco = 200;
        $prod->save();
        $prod->categorias()->attach($moveis->id);
        $prod->caracteristicas()->attach($cor->id, ['valor' => 'Marrom']);
        $prod->caracteristicas()->attach($altura->id, ['valor' => '91 cm']);
        $prod->caracteristicas()->attach($largura->id, ['valor' => '42 cm']);
        $prod->caracteristicas()->attach($profundidade->id, ['valor' => '48 cm']);
        $prod->caracteristicas()->attach($peso->id, ['valor' => '7,600 kg']);
        
        
        
        $prod = new Produto();
        $prod->uuid = Uuid::uuid4()->toString();
        $prod->nome = "Mesa para Escritório Borda Rígida 126x68 cm - Cinza e Preto";
        $prod->descricao = "A Mesa para Escritório com Borda Rígida 126 x 68 cm é um clássico quando falamos em móveis para escritório. Com um design reto e discreto, ela deixa o ambiente profissional e muito elegante.";
        $prod->imagem = "https://staticmobly.akamaized.net/p/PRADO-Mesa-para-EscritC3B3rio-Borda-RC3ADgida-126x68-cm---Cinza-e-Preto-2697-913705-1-zoom.jpg";
        $prod->preco = 800;
        $prod->save();
        $prod->categorias()->attach($moveis->id);
        $prod->categorias()->attach($moveis->id);
        $prod->caracteristicas()->attach($cor->id, ['valor' => 'Cinza e Preto']);
        $prod->caracteristicas()->attach($altura->id, ['valor' => '74 cm']);
        $prod->caracteristicas()->attach($largura->id, ['valor' => '125 cm']);
        $prod->caracteristicas()->attach($profundidade->id, ['valor' => '68 cm']);
        $prod->caracteristicas()->attach($peso->id, ['valor' => '14,000 kg']);
        
        $prod = new Produto();
        $prod->uuid = Uuid::uuid4()->toString();
        $prod->nome = "Abajur 50 Wengue Para 1 lâmpada";
        $prod->descricao = "Para criar um ambiente mais iluminado e ainda fornecer uma decoração diferenciada ao cômodo, o Abajur 50 Wengue é uma ótima opção. O acessório possui um belo design e deixa o visual mais agradável e convidativo. A peça bivolt tem a cúpula feita na cor branca e a base marrom, unindo o rústico e o clean em um só produto. Uma graça! : )";
        $prod->imagem = "https://staticmobly.akamaized.net/p/Jd-Molina-Abajur-50-Wengue-Para-1-lC3A2mpada-5172-803661-1-zoom.jpg";
        $prod->preco = 127.99;
        $prod->save();
        $prod->categorias()->attach($decoracao->id);
        $prod->caracteristicas()->attach($cor->id, ['valor' => 'Branco']);
        $prod->caracteristicas()->attach($altura->id, ['valor' => '40 cm']);
        $prod->caracteristicas()->attach($largura->id, ['valor' => '22 cm']);
        $prod->caracteristicas()->attach($profundidade->id, ['valor' => '22 cm']);
        $prod->caracteristicas()->attach($peso->id, ['valor' => '0,865 kg']);
        
        
        
        $prod = new Produto();
        $prod->uuid = Uuid::uuid4()->toString();
        $prod->nome = "Playstation Two";
        $prod->descricao = "O PlayStation 2 possui um design totalmente arrojado: é elegante e fino como um DVD Player e é capaz de rodar filmes com a mesma perfeição que o aparelho eletrônico. Com uma extensa biblioteca de jogos – mais de 1800 títulos disponíveis -, muitos dos quais já estão em rede para competição on-line, o PS2 já vem com DVD/CD (Região 1) incorporado, oferecendo um recurso de vídeo completo e sofisticado.";
        $prod->imagem = "https://www.feiradosimportados.com.br/wp-content/uploads/2016/03/Playstation-2.jpg";
        $prod->preco = 400;
        $prod->save();
        $prod->categorias()->attach($eletronicos->id);
        $prod->caracteristicas()->attach($cor->id, ['valor' => 'Preto']);
        
        
        $prod = new Produto();
        $prod->uuid = Uuid::uuid4()->toString();
        $prod->nome = "Playstation Three";
        $prod->descricao = "O PlayStation 3, também conhecido apenas por PS3, é o terceiro console de jogos produzido pela Sony, oficialmente lançado para o público em 11 de novembro de 2006. Ele concorre diretamente com o Xbox 360 e o Wii, todos dentro da sétima geração deste gênero de aparelho. Hoje, são mais de 22 milhões de unidades vendidas em todo o mundo.";
        $prod->imagem = "http://s2.glbimg.com/jXUUcTaNyUxsSeu4qxInv6aczlk=/0x600/s.glbimg.com/po/tt2/f/original/2014/12/26/play3.jpg";
        $prod->preco = 1000;
        $prod->save();
        $prod->categorias()->attach($eletronicos->id);
        $prod->caracteristicas()->attach($cor->id, ['valor' => 'Preto']);
        
        $prod = new Produto();
        $prod->uuid = Uuid::uuid4()->toString();
        $prod->nome = "Geladeira Brastemp";
        $prod->descricao = "O Refrigerador BRM56AB da Brastemp foi produzido especialmente pra você! Conta com vários diferenciais que vão te encantar, como a função Turbo Ice onde você pode obter gelo mais rápido sempre que precisar. Twist Ice Advanced esse exclusivo sistema permite abastecer as formas de gelo de um jeito inteligente, evitando molhar o chão da cozinha, além de desenformar o gelo facilmente e armazená-lo em um recipiente portátil e prático. Espaço Adapt prateleiras com múltiplas combinações possíveis para armazenar itens de diversos tamanhos na porta de sua geladeira. Turbo Control compartimento extrafrio bipartido, com função turbo que permite aumentar a potência em um dos lados para gelar bebidas e sobremesas ainda mais rápido. Compartimento especial pra gelar mais rápido suas bebidas. Ele pode ser encaixado tanto no freezer como no refrigerado para gelar a cerveja ou o refrigerante, com um espaço perfeito para 03 garrafas long neck ou 06 latas. Ah, e além disso possui painel eletrônico a melhor maneira de acionar as funções da sua geladeira. Com este painel você pode controlar a temperatura do refrigerador, ser avisado quando a porta ficou aberta e escolher modos pré-programados para evitar funções especiais. Leve o melhor da Brastemp para sua casa!";
        $prod->imagem = "https://a-static.mlcdn.com.br/618x463/geladeira-refrigerador-brastemp-frost-free-duplex-462l-brm56abana-branca/magazineluiza/013082201/b0e3d8cb07da450678277f8c8595a112.jpg";
        $prod->preco = 1000;
        $prod->save();
        $prod->categorias()->attach($eletrodomesticos->id); 
        $prod->categorias()->attach($cozinha->id); 
        $prod->caracteristicas()->attach($cor->id, ['valor' => 'Branco']);
        $prod->caracteristicas()->attach($altura->id, ['valor' => '140 cm']);
        $prod->caracteristicas()->attach($largura->id, ['valor' => '100 cm']);
        $prod->caracteristicas()->attach($profundidade->id, ['valor' => '70 cm']);
        $prod->caracteristicas()->attach($peso->id, ['valor' => '20,000 kg']);
        
        
        $prod = new Produto();
        $prod->uuid = Uuid::uuid4()->toString();
        $prod->nome = "Forno de Embutir Elétrico Brastemp Ative!";
        $prod->descricao = "Forno Elétrico de embutir Brastemp Ative, acabamento em inox com painel de controle touch screen de última geração. Timer digital em 3 funções - permite programar o horário que o forno deve ser ligado, o tempo do assamento e o término do preparo. No término do tempo programado, o forno desliga automaticamente e emite um sinal sonoro. Nove funções pré-programadas de acionamento rápido, garantindo muito mais praticidade no seu dia a dia, dentre elas as Funções Descongelar e Turbo Grill a um simples clique. E a Função assar 2 pratos ao mesmo tempo - a convecção com resistência de calor permite assar 2 pratos simultaneamente sem que haja necessidade de inverter a posição nas prateleiras. Se forem 2 assados iguais (ex: 2 pizzas ou 2 tortas) ambos serão assados no mesmo tempo de duração.";
        $prod->imagem = "https://a-static.mlcdn.com.br/618x463/forno-de-embutir-eletrico-brastemp-ative-bo360arrna-inox-60l-grill-timer/magazineluiza/200126300/157707798ed556dadb948ea5d2af7981.jpg";
        $prod->preco = 300;
        $prod->save();
        $prod->categorias()->attach($eletrodomesticos->id); 
        $prod->categorias()->attach($cozinha->id); 
        $prod->caracteristicas()->attach($cor->id, ['valor' => 'Prata']);
        $prod->caracteristicas()->attach($altura->id, ['valor' => '100 cm']);
        $prod->caracteristicas()->attach($largura->id, ['valor' => '50 cm']);
        $prod->caracteristicas()->attach($profundidade->id, ['valor' => '50 cm']);
        $prod->caracteristicas()->attach($peso->id, ['valor' => '10,000 kg']);
        
        $prod = new Produto();
        $prod->uuid = Uuid::uuid4()->toString();
        $prod->nome = "Micro-ondas Electrolux MEO44 - 34L";
        $prod->descricao = "O Micro-ondas Electrolux MEO44 com capacidade de 34 Litros, vai ser um auxílio e tanto no preparo de suas receitas. Com ele você pode fazer desde receitas do dia a dia como um menu Light com receitas mais saudáveis. E se por acaso ficar aquele odor, com a função tirar odor voc~e não vai ter mais esse problema, deixando sua cozinha sempre cheirosa e seu micro-ondas sempre limpinho. isso sem falar que você pode assistir aos vídeos no seu celular com o QR Code e prepare as receitas preferidas das crianças com o Menu Kids, com as receitas preferidas dos pequenos, como pipoca e brigadeiro. E a para facilitar ainda mais ele possui a função manter aquecido, seus pratos ficam na temperatura ideal.";
        $prod->imagem = "https://a-static.mlcdn.com.br/618x463/micro-ondas-electrolux-meo44-34l/magazineluiza/011398601/a935ea17cbf53516d89f0b667867b77d.jpg";
        $prod->preco = 500;
        $prod->save();
        $prod->categorias()->attach($eletrodomesticos->id);
        $prod->categorias()->attach($cozinha->id); 
        $prod->caracteristicas()->attach($cor->id, ['valor' => 'Branco']);
        $prod->caracteristicas()->attach($altura->id, ['valor' => '50 cm']);
        $prod->caracteristicas()->attach($largura->id, ['valor' => '50 cm']);
        $prod->caracteristicas()->attach($profundidade->id, ['valor' => '50 cm']);
        $prod->caracteristicas()->attach($peso->id, ['valor' => '3,000 kg']);
        
        $prod = new Produto();
        $prod->uuid = Uuid::uuid4()->toString();
        $prod->nome = "Jogo de Banho KNUT Royal 4 Peças Verde Claro – Santista";
        $prod->descricao = "Com esse belíssimo Jogo de Banho KNUT Royal 4 Peças você leva para seu banheiro muito mais qualidade e conforto, além de um toque de beleza e elegância à decoração do ambiente.";
        $prod->imagem = "https://staticmobly.akamaized.net/p/Santista-Jogo-de-Banho-KNUT-Royal-4-PeC3A7as-Verde-Claro-E28093-Santista-8565-758163-1-zoom.jpg";
        $prod->preco = 59.9;
        $prod->save();
        $prod->categorias()->attach($banho->id);
        $prod->caracteristicas()->attach($cor->id, ['valor' => 'Verde Claro']);
        $prod->caracteristicas()->attach($altura->id, ['valor' => '2 cm']);
        $prod->caracteristicas()->attach($largura->id, ['valor' => '70 cm']);
        $prod->caracteristicas()->attach($profundidade->id, ['valor' => '130 cm']);
        $prod->caracteristicas()->attach($peso->id, ['valor' => '2,000 kg']);
        
    }
}
