<?php

use Illuminate\Database\Seeder;
use Mobly\Persistence\Eloquent\Model\Categoria;
use Ramsey\Uuid\Uuid;

class CategoriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $c = new Categoria();
        $c->uuid = Uuid::uuid4()->toString();
        $c->nome = "Móveis";
        $c->save();
        
        $c = new Categoria();
        $c->uuid = Uuid::uuid4()->toString();
        $c->nome = "Eletrônicos";
        $c->save();
        
        $c = new Categoria();
        $c->uuid = Uuid::uuid4()->toString();
        $c->nome = "Eletrodomésticos";
        $c->save();
        
        $c = new Categoria();
        $c->uuid = Uuid::uuid4()->toString();
        $c->nome = "Banho";
        $c->save();
        
        $c = new Categoria();
        $c->uuid = Uuid::uuid4()->toString();
        $c->nome = "Decoração";
        $c->save();
        
        $c = new Categoria();
        $c->uuid = Uuid::uuid4()->toString();
        $c->nome = "Armário";
        $c->save();
        
        $c = new Categoria();
        $c->uuid = Uuid::uuid4()->toString();
        $c->nome = "Cozinha";
        $c->save();
    }
}