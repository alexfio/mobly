<?php

use Illuminate\Database\Seeder;
use Mobly\Persistence\Eloquent\Model\FormaPagamento;
use Ramsey\Uuid\Uuid;

class FormasPagamentoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $f = new FormaPagamento();
        $f->uuid = Uuid::uuid4()->toString();
        $f->nome = "Cartão de Crédito";
        $f->save();
        
        $f = new FormaPagamento();
        $f->uuid = Uuid::uuid4()->toString();
        $f->nome = "Boleto Bancário";
        $f->save();
    }
}
