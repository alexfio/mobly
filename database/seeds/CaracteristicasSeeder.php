<?php

use Illuminate\Database\Seeder;
use Mobly\Persistence\Eloquent\Model\Caracteristica;
use Ramsey\Uuid\Uuid;

class CaracteristicasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $c = new Caracteristica();
        $c->uuid = Uuid::uuid4()->toString();
        $c->nome = "Cor";
        $c->save();
        
        $c = new Caracteristica();
        $c->uuid = Uuid::uuid4()->toString();
        $c->nome = "Material";
        $c->save();
        
        $c = new Caracteristica();
        $c->uuid = Uuid::uuid4()->toString();
        $c->nome = "Altura";
        $c->save();
        
        $c = new Caracteristica();
        $c->uuid = Uuid::uuid4()->toString();
        $c->nome = "Largura";
        $c->save();
        
        $c = new Caracteristica();
        $c->uuid = Uuid::uuid4()->toString();
        $c->nome = "Profundidade";
        $c->save();
        
        $c = new Caracteristica();
        $c->uuid = Uuid::uuid4()->toString();
        $c->nome = "Peso";
        $c->save();
    }
}