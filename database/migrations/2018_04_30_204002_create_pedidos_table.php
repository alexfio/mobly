<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pedidos', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->uuid('uuid');
            $table->integer('cliente_id')->unsigned();
            $table->integer('estado_id')->unsigned();
            $table->string('endereco_completo');
            $table->string('numero');
            $table->string('cidade');
            $table->string('cep');
            $table->integer('forma_pagamento_id')->unsigned();
            $table->text('cartao_credito')->nullable();
            $table->timestamps();
            
            $table->unique('uuid');
        });
        
        Schema::table('pedidos', function(Blueprint $table){
            $table->foreign('cliente_id')
                  ->references('id')
                  ->on('clientes');
            
            $table->foreign('estado_id')
                  ->references('id')
                  ->on('estados');
            
            
            $table->foreign('forma_pagamento_id')
                  ->references('id')
                  ->on('forma_pagamentos');
            
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pedidos', function(Blueprint $table){
            $table->dropForeign(['cliente_id']);
            $table->dropForeign(['forma_pagamento_id']);
            $table->dropForeign(['estado_id']);
            $table->dropUnique(['uuid']);
        });
        
        Schema::dropIfExists('pedidos');
    }
}
