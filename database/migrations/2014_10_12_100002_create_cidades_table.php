<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCidadesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cidades', function (Blueprint $table) {
            
            $table->increments('id')->unsigned();
            $table->uuid('uuid');
            $table->integer('estado_id')->unsigned();
            $table->string('nome');
            
            $table->unique('uuid');      
            
        });
        
        Schema::table('cidades', function(Blueprint $table) {
              $table->foreign('estado_id')
                    ->references('id')
                    ->on('estados')
                    ->onUpdate('restrict')  
                    ->onDelete('restrict');
        });
    }


    public function down()
    {   
    
        Schema::table('cidades', function(Blueprint $table) {
            $table->dropForeign(['estado_id']);
            $table->dropUnique(['uuid']);
        });
        
        Schema::dropIfExists('cidades');
    }
}