<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriaProdutoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categoria_produto', function (Blueprint $table) {
            $table->integer('categoria_id')->unsigned();
            $table->integer('produto_id')->unsigned();
        });
        
        Schema::table('categoria_produto', function(Blueprint $table) {
            $table->foreign('categoria_id')
                  ->references('id')
                  ->on('categorias')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            
            $table->foreign('produto_id')
                  ->references('id')
                  ->on('produtos')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('categoria_produto', function(Blueprint $table) {
            $table->dropForeign(['categoria_id']);
            $table->dropForeign(['produto_id']);
        });
        
        Schema::dropIfExists('categoria_produto');
    }
}
