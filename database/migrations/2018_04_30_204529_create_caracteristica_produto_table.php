<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCaracteristicaProdutoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('caracteristica_produto', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->integer('caracteristica_id')->unsigned();
            $table->integer('produto_id')->unsigned();
            $table->text('valor');
        });
        
        Schema::table('caracteristica_produto', function(Blueprint $table) {
            $table->foreign('caracteristica_id')
                  ->references('id')
                  ->on('caracteristicas')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
            
            $table->foreign('produto_id')
                  ->references('id')
                  ->on('produtos')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('caracteristica_produto', function(Blueprint $table) {
            $table->dropForeign(['caracteristica_id']);
            $table->dropForeign(['produto_id']);
        });
        
        Schema::dropIfExists('caracteristica_produto');
    }
}
