<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemPedidosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item_pedidos', function (Blueprint $table) {
            $table->increments('id')->unsigned();
            $table->uuid('uuid');
            $table->integer('pedido_id')->unsigned();
            $table->integer('produto_id')->unsigned();
            $table->integer('quantidade')->unsigned();
            $table->decimal('preco_venda', 8, 2);
            $table->timestamps();
            $table->unique('uuid');
        });
        
        Schema::table('item_pedidos', function(Blueprint $table){
            $table->foreign('pedido_id')
                  ->references('id')
                  ->on('pedidos')
                  ->onDelete('cascade')
                  ->onUpdate('cascade');  
        });
        
        Schema::table('item_pedidos', function(Blueprint $table){
            $table->foreign('produto_id')
                  ->references('id')
                  ->on('produtos')
                  ->onDelete('restrict')
                  ->onUpdate('cascade');  
        });
        
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('item_pedidos', function(Blueprint $table){
            $table->dropForeign(['pedido_id']);
            $table->dropForeign(['produto_id']);
            $table->dropUnique(['uuid']);
        });
        
        Schema::dropIfExists('item_pedidos');
    }
}
