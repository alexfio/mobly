<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', "HomeController@index")->name('produtos');

Route::get('/categorias', "HomeController@verCategorias")
        ->name('categorias');


Route::get('/detalhes-produto/{uuid}', "HomeController@verDetalhesProduto")
        ->name('produtos-detalhes');

Route::get('/carrinho', "CarrinhoController@index")
        ->name('carrinho');

Route::get('/informacoes-pedido', "CarrinhoController@verInformacoesPedido")
        ->name('informacoes-pedido');
        
Route::get('/buscar-produto-categoria/{uuid}', "HomeController@exibirProdutosPorCategoria")
        ->name('buscar-produtos-categoria');

Route::post('/adicionar-ao-carrinho', "CarrinhoController@adicionarProdutoAoCarrinho")
        ->name('adicionar-ao-carrinho');

Route::get('/remover-do-carrinho', "CarrinhoController@removerDoCarrinho")
        ->name('remover-do-carrinho');

Route::match(['post', 'get'],'/buscar-por-nome', "HomeController@buscarPorNome")
        ->name('buscar-por-nome');


Route::post('/preparar-pedido', "CarrinhoController@prepararPedido")
        ->name('preparar-pedido');

Route::post('/processar-pedido', "CarrinhoController@processarPedido")
        ->name('processar-pedido');


