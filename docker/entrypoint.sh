#!/bin/bash

a2ensite localhost.conf
a2enmod rewrite

chmod 777 -R storage/logs && chmod 777 -R storage/framework/ && \
chmod 777 -R bootstrap/cache 
   
exec /usr/sbin/apache2ctl -DFOREGROUND


  
